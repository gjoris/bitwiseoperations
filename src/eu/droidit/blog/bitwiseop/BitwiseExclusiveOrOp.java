package eu.droidit.blog.bitwiseop;

import static eu.droidit.blog.bitwiseop.util.Formatter.tabs;
import static eu.droidit.blog.bitwiseop.util.Formatter.toBinaryStringWithLeadingZeros;

/**
 * This class will create 2 integers, and do a bitwise exclusive OR operation
 * <p/>
 * What is a bitwise exclusive OR operation?
 * When any of the bits in the same "place" are 1, the result of the exclusive OR operation is also 1.
 * Only if all bits are 0, the result will be zero
 * eg:
 * 1001
 * 0011
 * ----
 * 1011 <-- result
 *
 * @author Geroen Joris
 * @author DroidIT bvba
 * @link http://blog.droidit.eu/
 */
public class BitwiseExclusiveOrOp {

    /*
    * This method will print out both integers in binary form, and the result in binary as well as "regular" form.
    * The result has been aligned, so the result easily deductable.
    */
    public static void main(String[] args) {

        int first = 54321;
        int second = 12345;
        System.out.println("Bit representation of first: " + tabs(10) + toBinaryStringWithLeadingZeros(first));
        System.out.println("Bit representation of second: " + tabs(10) + toBinaryStringWithLeadingZeros(second));

        int resultOfAndOperation = first | second;
        System.out.println("Bitwise exclusive OR operation result in binary representation: " + tabs(1) + toBinaryStringWithLeadingZeros(resultOfAndOperation));
        System.out.println("Bitwise exclusive OR in normal integer representation: " + tabs(5) + resultOfAndOperation);

    }

}
