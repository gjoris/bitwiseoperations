package eu.droidit.blog.bitwiseop;

/**
 * This class will print all integral types in binary form.
 * The print methods for byte, short and char can be left out, since they will use the print method for int
 *
 * @author Geroen Joris
 * @author DroidIT bvba
 * @link http://blog.droidit.eu/
 */

public class BinaryStringPrinter {

    public static void main(String[] args) {
        byte b = -12;
        short s = 200;
        char c = '\uff00';
        int i = 35000;
        long l = 21474836470l;
        printBinaryString(b);
        printBinaryString(s);
        printBinaryString(c);
        printBinaryString(i);
        printBinaryString(l);
    }

    /*
    Not necessary, could leave this out, and printBinaryString(short s) or printBinaryString(int i) will be used
    Same result will be yielded
     */
    private static void printBinaryString(byte b) {
        System.out.println("Printing byte: " + Integer.toBinaryString(b));
    }

    /*
    Not necessary, could leave this out, and printBinaryString(int i) will be used
    Same result will be yielded
     */
    private static void printBinaryString(short s) {
        System.out.println("Printing short: " + Integer.toBinaryString(s));
    }

    /*
    Not necessary, could leave this out, and printBinaryString(int i) will be used
    Same result will be yielded
     */
    private static void printBinaryString(char c) {
        System.out.println("Printing char: " + Integer.toBinaryString(c));
    }

    private static void printBinaryString(int i) {
        System.out.println("Printing int: " + Integer.toBinaryString(i));
    }

    private static void printBinaryString(long l) {
        System.out.println("Printing long: " + Long.toBinaryString(l));
    }

}
