package eu.droidit.blog.bitwiseop.util;

/**
 * Formatter for the article on bitwise and bitshift operations
 *
 * @author Geroen Joris
 * @author DroidIT bvba
 * @link http://blog.droidit.eu/
 */
public class Formatter {

    public static String tabs(int number) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= number; i++) {
            sb.append("\t");
        }
        return sb.toString();
    }

    /*
    * Normally, a binary string will only be shown starting from the first 0. These strings have been formatted to show
    * all leading 0's for the 32-bit integer.
    *
     */
    public static String toBinaryStringWithLeadingZeros(int value) {
        return String.format("%32s", Integer.toBinaryString(value)).replace(' ', '0');
    }
}
