package eu.droidit.blog.bitwiseop;

import static eu.droidit.blog.bitwiseop.util.Formatter.tabs;
import static eu.droidit.blog.bitwiseop.util.Formatter.toBinaryStringWithLeadingZeros;

/**
 * This class will create 2 integers, and do a bitwise inclusive OR operation
 * <p/>
 * What is a bitwise inclusive OR operation?
 * When only one of the 2 bits in the same "place" is 1, the result of the inclusive OR operation is also 1.
 * If all bits are 0, the result will be zero
 * If all bits are 1, the result will be zero
 * eg:
 * 1001
 * 0011
 * ----
 * 1010 <-- result
 *
 * @author Geroen Joris
 * @author DroidIT bvba
 * @link http://blog.droidit.eu/
 */
public class BitwiseInclusiveOrOp {

    /*
    * This method will print out both integers in binary form, and the result in binary as well as "regular" form.
    * The result has been aligned, so the result easily deductable.
    */
    public static void main(String[] args) {

        int first = 54321;
        int second = 12345;
        System.out.println("Bit representation of first: " + tabs(10) + toBinaryStringWithLeadingZeros(first));
        System.out.println("Bit representation of second: " + tabs(10) + toBinaryStringWithLeadingZeros(second));

        int resultOfAndOperation = first ^ second;
        System.out.println("Bitwise inclusive OR operation result in binary representation: " + tabs(1) + toBinaryStringWithLeadingZeros(resultOfAndOperation));
        System.out.println("Bitwise inclusive OR in normal integer representation: " + tabs(5) + resultOfAndOperation);

    }

}
