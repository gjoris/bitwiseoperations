package eu.droidit.blog.bitwiseop;

import static eu.droidit.blog.bitwiseop.util.Formatter.tabs;
import static eu.droidit.blog.bitwiseop.util.Formatter.toBinaryStringWithLeadingZeros;

/**
 * This class will create an integer, and calculate its bitwise complement.
 * <p/>
 * What is a bitwise complement?
 * All the bits are inverted in this complement, which basicly means:
 * - all 1's become 0's
 * - all 0's become 1's
 * Given an integral x, represented by 010101, its binary complement is 101010.
 *
 * @author Geroen Joris
 * @author DroidIT bvba
 * @link http://blog.droidit.eu/
 */
public class BitwiseUnaryComplementOp {

    /*
    * This method will print out the original integer in binary form, and the result in binary as well as "regular" form.
    * The result has been aligned, so the result is easily deductable.
    */
    public static void main(String[] args) {
        int first = 54321;
        int result = ~first;
        System.out.println("Binary representation of first: " + tabs(3) + toBinaryStringWithLeadingZeros(first));
        System.out.println("Binary representation of unary complement: " + tabs(1) + toBinaryStringWithLeadingZeros(result));
        System.out.println("Normal representation of first: " + tabs(3) + first);
        System.out.println("Normal representation of unary complement: " + tabs(1) + result);
    }

}
