package eu.droidit.blog.bitwiseop;

import static eu.droidit.blog.bitwiseop.util.Formatter.tabs;
import static eu.droidit.blog.bitwiseop.util.Formatter.toBinaryStringWithLeadingZeros;

/**
 * This class will create 2 integers, and do a bitwise AND operation
 * <p/>
 * What is a bitwise AND operation?
 * When both bits in the same "place" are 1, the result of the AND operation is also 1.
 * In all other circumstances, the result of the AND operation is 0.
 * eg:
 * 101
 * 011
 * ---
 * 001 <-- result
 *
 * @author Geroen Joris
 * @author DroidIT bvba
 * @link http://blog.droidit.eu/
 */
public class BitwiseAndOp {

    /*
    * This method will print out both integers in binary form, and the result in binary as well as "regular" form.
    * The result has been aligned, so the result easily deductable.
     */

    public static void main(String[] args) {

        int first = 54321;
        int second = 12345;
        System.out.println("Bit representation of first: " + tabs(7) + toBinaryStringWithLeadingZeros(first));
        System.out.println("Bit representation of second: " + tabs(7) + toBinaryStringWithLeadingZeros(second));

        int resultOfAndOperation = first & second;
        System.out.println("Bitwise AND operation result in binary representation: " + tabs(1) + toBinaryStringWithLeadingZeros(resultOfAndOperation));
        System.out.println("Bitwise AND operation in normal integer representation: " + tabs(1) + resultOfAndOperation);

    }

}
