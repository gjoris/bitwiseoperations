package eu.droidit.blog.bitwiseop;

import static eu.droidit.blog.bitwiseop.util.Formatter.tabs;
import static eu.droidit.blog.bitwiseop.util.Formatter.toBinaryStringWithLeadingZeros;

/**
 * This class will create 2 integers, and do a unsigned right bitshift of the first, for the value of the second.
 * <p/>
 * What is a unsigned bitshift to the left?
 * Given this function:
 * x >>> y
 * all bits of x are shifted to the right, for a length of y. A 0 is added to the leftmost position, making all resulting
 * integrals positive.
 *
 * @author Geroen Joris
 * @author DroidIT bvba
 * @link http://blog.droidit.eu/
 */
public class BitshiftUnsignedToRightOp {

    /*
    * This method will print out the original integer in binary form, and the result in binary as well as "regular" form.
    * The result has been aligned, so the result is easily deductable.
    */
    public static void main(String[] args) {

        int first = Integer.MIN_VALUE;
        int second = 1;
        System.out.println("Bit representation of initial: " + tabs(10) + toBinaryStringWithLeadingZeros(first));

        int resultOfAndOperation = first >>> second;
        System.out.println("Unsigned right bitshift operation result in binary representation: " + tabs(1) + toBinaryStringWithLeadingZeros(resultOfAndOperation));
        System.out.println("Unsigned right bitshift operation in normal integer representation: " + tabs(5) + resultOfAndOperation);

    }
}
